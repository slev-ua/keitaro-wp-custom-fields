<?php
/*
 * Plugin Name: Custom Fields into Rest Api
 * Description: Позволяет добавлять нужные поля через rest api
 */

add_action( 'rest_api_init', 'action_function_register_metas' );

function action_function_register_metas( $wp_rest_server )
{
	
	$object_type = 'post';
	$args1 = array(
		'type'         => 'string',
		'description'  => 'custom2',
		'single'       => true,
		'show_in_rest' => true,
	);
	
	register_meta( $object_type, 'reviews_avg', $args1 );
	register_meta( $object_type, 'reviews_avg2', $args1 );
	register_meta( $object_type, 'reviews_marks2', $args1 );
	register_meta( $object_type, 'tds_link', $args1 );
}
